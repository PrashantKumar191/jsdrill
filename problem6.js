function problem6(inventory) {
  let emptyArr = [];

  // checking for undefined
  if (!inventory || typeof inventory !== "object") {
    return emptyArr;
  }

  // checking for empty {} or empty[]
  if (Object.keys(inventory).length === 0) {
    return emptyArr;
  }

  let BMWAndAudi = [];
  for (let x = 0; x < inventory.length - 1; x++) {
    if (inventory[x].car_make === "BMW" || inventory[x].car_make === "Audi") {
      BMWAndAudi.push(inventory[x]);
    }
  }
  return BMWAndAudi;
}

module.exports = problem6;
