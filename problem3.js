function problem3(inventory) {
  let emptyArr = [];

  if (!inventory || typeof inventory !== "object") {
    return emptyArr;
  }

  if (Object.keys(inventory).length === 0) {
    return emptyArr;
  }

  let x = [];
  for (let i = 0; i < inventory.length; i++) {
    if (Object.keys(inventory[i]).length === 0) {
      return emptyArr;
    } else {
      x.push(inventory[i].car_model.toLowerCase());
    }
  }

  x.sort();

  let result = [];

  for (let i = 0; i < x.length; i++) {
    for (let j = 0; j < inventory.length; j++) {
      if (x[i] == inventory[j].car_model.toLocaleLowerCase()) {
        result.push(inventory[j].car_model);
        inventory[j].car_model = "xxxxxx";
        break;
      }
    }
  }
  return result;
}

module.exports = problem3;
