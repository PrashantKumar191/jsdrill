function problem4(inventory) {
  let emptyArr = [];

  // checking for undefined
  if (!inventory || typeof inventory !== "object") {
    return emptyArr;
  }

  // checking for empty {}
  if (Object.keys(inventory).length === 0) {
    return emptyArr;
  }

  // searching for  years
  let yearArray = [];
  for (let i = 0; i < inventory.length; i++) {
    yearArray.push(inventory[i].car_year);
  }
  return yearArray;
}

module.exports = problem4;
