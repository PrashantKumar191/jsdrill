function problem5(inventory, arr) {
  let emptyArr = [];

  if (!inventory || !arr) {
    return emptyArr;
  }

  if (typeof inventory !== "object") {
    return emptyArr;
  }

  if (Object.keys(inventory).length === 0) {
    return emptyArr;
  }

  let yearLess2000 = [];
  for (let x = 0; x < arr.length; x++) {
    if (arr[x] < 2000) {
      yearLess2000.push(arr[x]);
    }
  }
  return yearLess2000;
}
module.exports = problem5;
