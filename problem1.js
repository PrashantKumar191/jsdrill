function problem1(inventory, requiredId) {
  let emptyArr = [];

  if (!inventory || !requiredId) {
    return emptyArr;
  }

  if (Object.keys(inventory).length === 0) {
    return emptyArr;
  }

  if (typeof inventory !== "object") {
    return emptyArr;
  }

  if (typeof requiredId !== "number" || requiredId < 0) {
    return emptyArr;
  }

  //searching for  required id

  for (let i = 0; i <= inventory.length - 1; i++) {
    if (inventory[i].id === requiredId) {
      return inventory[i];
    }
  }
  return emptyArr;
}

module.exports = problem1;
